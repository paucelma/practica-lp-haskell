# Pràctica de Haskell LP Q1 20/21: Decision Trees

## Descripció de la pràctica

Aquesta pràctica aplica un algorisme per crear arbres de decisions aplicats a predir si un bolet és verinós o no segons les seves característiques. És un projecte escrit en el llenguatge de programació funcional Haskell, que aplica els coneixements adquirits durant la primera meitat de l'assignatura.

## Funcionament

Un cop descomprimit l'arxiu ZIP (cosa que es suposa que s'ha aconseguit si l'usuari està llegint aquest fitxer), s'haurà de compilar el programa amb la comanda `ghc dts.hs`. S'ha de tenir el compilador de Haskell `ghc` instal·lat per poder compilar aquest projecte.

Ara, caldrà executar el programa escrivint `./dts` a la consola de comandes. El programa generarà un arbre de decisions i ens farà preguntes sobre el bolet que volem classificar. Segons les respostes que li donem, anirà recorrent l'arbre fins que arribi a una fulla i ens pugui donar classificar amb certesa el bolet.

## Estructura bàsica del projecte

El projecte està format per X blocs de codi dins l'arxiu `dts.hs`:

* La funció *main*
* L'estructura de dades *Bolet* i les seves funcions 
* L'estructura de dades *DecisionTree* i les seves funcions
* Bloc de funcions per fer parsing de l'arxiu de dades i transformar-lo en instàncies de *Bolet*
* Bloc de funcions dedicades a construir l'arbre de decisions a partir d'una llista *[Bolet]*
* Bloc de funcions dedicades a recòrrer l'arbre de decisions

Totes les funcions i estructures de dades estan documentades en més detall al propi codi.