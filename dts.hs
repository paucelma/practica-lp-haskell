-- Classe Bolet

-- Bolet: Consisteix d'una String que representa la seva classe (verinós o comestible)
-- i d'una llista d'atributs representats en format (nom, valor) 
data Bolet = Bolet String [(String, String)] 
	deriving (Show)

-- Retorna la classe (si és verinós o no) del bolet
getClass :: Bolet -> String
getClass (Bolet c _) = c

-- Retorna una llista amb els atributs del bolet en format (nom, valor)
getAttributes :: Bolet -> [(String, String)]
getAttributes (Bolet _ a) = a

-- Retorna una llista amb els noms dels atributs del bolet
getAttributeNames :: Bolet -> [String]
getAttributeNames (Bolet _ att) = map (fst) att

-- Retorna el valor de l'atribut amb el nom "name"
getAttributeValue :: String -> Bolet -> String
getAttributeValue name (Bolet _ att) = snd $ filter ((== name) . fst) att !! 0

-- Retorna una llista amb els valors dels atributs del bolet, a més de la seva classe fent una tupla amb cadascun d'ells
getAllClassAndAttributes :: Bolet -> [(String, String)]
getAllClassAndAttributes (Bolet c att) = [(c, snd y) | y <- att]

-- Elimina un atribut d'un bolet
deleteAttribute :: String -> Bolet -> Bolet
deleteAttribute name (Bolet c att) = (Bolet c new_att)
	where
		new_att = [y | y <- att, fst y /= name]

-- DecisionTree: Consisteix en una String, que representa el valor de l'arrel de l'arbre 
-- i en una llista d'arestes que porten a altres subarbres. Aquesta llista està representada
-- en tuples amb la forma (aresta, subarbre)
data DecisionTree = DecisionTree String [(String, DecisionTree)]

-- Implementació de "Show" per aquesta estructura de dades
instance Show DecisionTree where
	show (DecisionTree s l) = treeToString (DecisionTree s l) 0

-- Funció recursiva que imprimeix un nivell de l'arbre amb les tabulacions correctes
treeToString :: DecisionTree -> Int -> String
treeToString (DecisionTree s []) deep = tabs ++ s ++ "\n"
	where
		tabs = take deep (repeat '\t')
treeToString (DecisionTree s l) deep = tabs ++ s ++ "\n" ++ show_list
	where
		tabs = take deep (repeat '\t')
		show_list = foldl (++) "" [tabs ++ "\t" ++ fst y ++ "\n" ++ treeToString (snd y) (deep+2) | y <- l] 

-- Retorna la llista de branques de l'arbre
getBranches :: DecisionTree -> [(String, DecisionTree)]
getBranches (DecisionTree _ l) = l

-- Retorna el DecisionTree al que porta l'aresta amb valor "value"
getBranch :: DecisionTree -> String -> DecisionTree 
getBranch (DecisionTree _ l) value = branch
	where branch = [snd y | y <- l,  fst y == value] !! 0

main :: IO ()
main = do
	contents <- readFile "agaricus-lepiota.data"
	let bolets = (parseBolets . lines) contents
	let tree = constructTree bolets
	putStrLn $ show $ tree
	treeTraversal tree

-- Bloc amb funcions de parsing pel Bolet

-- Separa una String en una llista de caràcters, eliminant els que donen True la funció "f" 
splitWhen :: (Char -> Bool) -> String -> String
splitWhen f s = [c | c <- s, not(f c)]

-- Converteix un caràcter que representa la classe d'un bolet en una String amb la paraula "poisounous" o "edible"
parseClass :: Char -> String
parseClass c 
	| c == 'p' = "poisounous"
	| otherwise = "edible"

-- Converteix una String amb els valors dels atributs en una llista de tuples que utilitzarem per construïr un Bolet
parseAttributes:: String -> Int -> [(String,String)]
parseAttributes s i 
	| i == 1 = ("cap-shape",parseCapShape(h)):t
	| i == 2 = ("cap-surface",parseCapSurface(h)):t
	| i == 3 = ("cap-color",parseCapColor(h)):t
	| i == 4 = ("bruises?",parseBruises(h)):t
	| i == 5 = ("odor",parseOdor(h)):t
	| i == 6 = ("gill-attachment",parseGillAttachment(h)):t
	| i == 7 = ("gill-spacing",parseGillSpacing(h)):t
	| i == 8 = ("gill-size",parseGillSize(h)):t
	| i == 9 = ("gill-color",parseGillColor(h)):t
	| i == 10 = ("stalk-shape",parseStalkShape(h)):t
	| i == 11 = ("stalk-root",parseStalkRoot(h)):t
	| i == 12 = ("stalk-surface-above-ring",parseStalkSurfaceRing(h)):t
	| i == 13 = ("stalk-surface-below-ring",parseStalkSurfaceRing(h)):t
	| i == 14 = ("stalk-color-above-ring",parseStalkColorRing(h)):t
	| i == 15 = ("stalk-color-below-ring",parseStalkColorRing(h)):t
	| i == 16 = ("veil-type",parseVeilType(h)):t
	| i == 17 = ("veil-color",parseVeilColor(h)):t
	| i == 18 = ("ring-number",parseRingNumber(h)):t
	| i == 19 = ("ring-type",parseRingType(h)):t
	| i == 20 = ("spore-print-color",parseSporePrintColor(h)):t
	| i == 21 = ("population",parsePopulation(h)):t
	| i == 22 = ("habitat",parseHabitat(h)):t
	| i == 23 = []
	where
		h = head s
		t = parseAttributes (tail s) (i+1) 

-- Donada una String amb el format de l'arxiu de dades, la transforma en un Bolet
listToBolet :: String -> Bolet
listToBolet (x:xs) = Bolet (parseClass x) (parseAttributes xs 1) 

-- Donada una llista de Strings amb el format de l'arxiu de dades, la transforma en una llista de Bolets
parseBolets :: [String] -> [Bolet]
parseBolets linies = map (listToBolet . splitWhen (==',')) linies

-- Bloc per passar els caràcters de l'arxiu de dades al seu significat descrit a l'arxiu "names"

parseCapShape :: Char -> String
parseCapShape 'b' = "bell"
parseCapShape 'c' = "conical"
parseCapShape 'x' = "convex"
parseCapShape 'f' = "flat"
parseCapShape 'k' = "knobbed"
parseCapShape 's' = "sunken" 

parseCapSurface :: Char -> String
parseCapSurface 'f' = "fibrous"
parseCapSurface 'g' = "grooves"
parseCapSurface 'y' = "scaly"
parseCapSurface 's' = "smooth" 

parseCapColor :: Char -> String
parseCapColor 'n' = "brown"
parseCapColor 'b' = "buff"
parseCapColor 'c' = "cinnamon"
parseCapColor 'g' = "gray"
parseCapColor 'r' = "green"
parseCapColor 'p' = "pink"
parseCapColor 'u' = "purple"
parseCapColor 'e' = "red"
parseCapColor 'w' = "white"
parseCapColor 'y' = "yellow" 

parseBruises :: Char -> String
parseBruises 't' = "bruises"
parseBruises 'f' = "no"

parseOdor :: Char -> String
parseOdor 'a' = "almond"
parseOdor 'l' = "anise"
parseOdor 'c' = "creosote"
parseOdor 'y' = "fishy"
parseOdor 'f' = "foul"
parseOdor 'm' = "musty"
parseOdor 'n' = "none"
parseOdor 'p' = "pungent"
parseOdor 's' = "spicy"

parseGillAttachment :: Char -> String
parseGillAttachment 'a' = "attached"
parseGillAttachment 'd' = "descending"
parseGillAttachment 'f' = "free"
parseGillAttachment 'n' = "notched"


parseGillSpacing :: Char -> String
parseGillSpacing 'c' = "close" 
parseGillSpacing 'w' = "crowded" 
parseGillSpacing 'd' = "distant" 

parseGillSize :: Char -> String
parseGillSize 'b' = "broad" 
parseGillSize 'n' = "narrow" 

parseGillColor :: Char -> String
parseGillColor 'k' = "black" 
parseGillColor 'n' = "brown" 
parseGillColor 'b' = "buff" 
parseGillColor 'h' = "chocolate" 
parseGillColor 'g' = "gray" 
parseGillColor 'r' = "green" 
parseGillColor 'o' = "orange" 
parseGillColor 'p' = "pink" 
parseGillColor 'u' = "purple" 
parseGillColor 'e' = "red" 
parseGillColor 'w' = "white" 
parseGillColor 'y' = "yellow" 

parseStalkShape :: Char -> String
parseStalkShape 'e' = "enlarging"
parseStalkShape 't' = "tapering" 

parseStalkRoot :: Char -> String
parseStalkRoot 'b' = "bulbous"
parseStalkRoot 'c' = "club"
parseStalkRoot 'u' = "cup"
parseStalkRoot 'e' = "equal"
parseStalkRoot 'z' = "rhizomorphs"
parseStalkRoot 'r' = "rooted"
parseStalkRoot '?' = "missing"

parseStalkSurfaceRing :: Char -> String
parseStalkSurfaceRing 'f' = "fibrous" 
parseStalkSurfaceRing 'y' = "scaly" 
parseStalkSurfaceRing 'k' = "silky" 
parseStalkSurfaceRing 's' = "smooth" 

parseStalkColorRing :: Char -> String
parseStalkColorRing 'n' = "brown" 
parseStalkColorRing 'b' = "buff" 
parseStalkColorRing 'c' = "cinnamon" 
parseStalkColorRing 'g' = "gray" 
parseStalkColorRing 'o' = "orange" 
parseStalkColorRing 'p' = "pink" 
parseStalkColorRing 'e' = "red" 
parseStalkColorRing 'w' = "white" 
parseStalkColorRing 'y' = "yellow" 

parseVeilType :: Char -> String
parseVeilType 'p' = "partial" 
parseVeilType 'u' = "universal" 

parseVeilColor :: Char -> String
parseVeilColor 'n' = "brown" 
parseVeilColor 'o' = "orange" 
parseVeilColor 'w' = "white" 
parseVeilColor 'y' = "yellow" 

parseRingNumber :: Char -> String
parseRingNumber 'n' = "none" 
parseRingNumber 'o' = "one" 
parseRingNumber 't' = "two" 

parseRingType :: Char -> String
parseRingType 'c' = "cobwebby" 
parseRingType 'e' = "evanescent" 
parseRingType 'f' = "flaring" 
parseRingType 'l' = "large" 
parseRingType 'n' = "none" 
parseRingType 'p' = "pendant" 
parseRingType 's' = "sheathing" 
parseRingType 'z' = "zone" 

parseSporePrintColor :: Char -> String
parseSporePrintColor 'k' = "black"
parseSporePrintColor 'n' = "brown"
parseSporePrintColor 'b' = "buff"
parseSporePrintColor 'h' = "chocolate"
parseSporePrintColor 'r' = "green"
parseSporePrintColor 'o' = "orange"
parseSporePrintColor 'u' = "purple"
parseSporePrintColor 'w' = "white"
parseSporePrintColor 'y' = "yellow"

parsePopulation :: Char -> String
parsePopulation 'a' = "abundant" 
parsePopulation 'c' = "clustered" 
parsePopulation 'n' = "numerous" 
parsePopulation 's' = "scattered" 
parsePopulation 'v' = "several" 
parsePopulation 'y' = "solitary" 

parseHabitat :: Char -> String
parseHabitat 'g' = "grasses" 
parseHabitat 'l' = "leaves" 
parseHabitat 'm' = "meadows" 
parseHabitat 'p' = "paths" 
parseHabitat 'u' = "urban" 
parseHabitat 'w' = "waste" 
parseHabitat 'd' = "woods" 

-- Bloc de funcions que es dediquen a construir l'arbre de decisions

-- Construeix l'arbre de decisions a partir d'una llista de bolets
constructTree :: [Bolet] -> DecisionTree
constructTree bolets = DecisionTree (fst max_accurate) (fulles ++ nodes) 
	where
		attributes = getAllAttributeValues bolets	
		max_accurate = maxByFunction (getAccuracy . snd) attributes
		valors_fulles = (getUniqueValues . getAccurateValues . snd) max_accurate
		fulles = [(snd y, (DecisionTree (fst y) []))| y <- valors_fulles]
		valors_nodes = [snd y | y <- (getUniqueValues . snd) max_accurate, not $ elem (snd y) (map (snd) valors_fulles)]
		filtrar_bolets = (filterBolets bolets (fst max_accurate))
		nodes = [(y, (constructTree . filtrar_bolets) y) | y <- valors_nodes] 

-- Per una llista de bolets, retorna només els que no tenen l'atribut "name" amb valor "value".
-- A més, elimina l'atribut "name" de tots els bolets de la llista que retorna 
filterBolets :: [Bolet] -> String -> String -> [Bolet]
filterBolets bolets name value = map (deleteAttribute name) new_bolets
	where
		new_bolets = [y | y <- bolets, not $ (getAttributeValue name y) /= value]

-- Per una llista de valors d'un atribut amb format (classe, valor) retorna aquells que retornen 1 a la funció "getAccuracy". 
-- És a dir, els que formaran fulles a l'arbre de decisions.
getAccurateValues :: [(String, String)] -> [(String, String)]
getAccurateValues xs = acc
	where
		acc = foldl (++) [] acc_lists
		acc_lists = [x | x <- lists, getAccuracy x == 1.0]
		lists = separateByValue xs

-- D'una llista de valors d'un atribut amb format (classe, valor) retorna elements de la llista amb "valor" únic
getUniqueValues :: [(String, String)] -> [(String, String)]
getUniqueValues [] = []
getUniqueValues (x:xs) = (x:getUniqueValues resta)
	where
		resta = [y | y <- xs, snd y /= snd x]

--Retorna el màxim d'una llista d'elements a segons una funció (a -> b), on b és un tipus ordenable
maxByFunction :: Ord b => (a -> b) -> [a] -> a
maxByFunction _ [x] = x
maxByFunction f (x:xs)
	| f x > f maxim = x
	| otherwise = maxim
	where
		maxim = maxByFunction (f) xs

--Obté una llista de tuples que representa les classes dels bolets i els valors d'un atribut
--Retorna l'accuracy tal i com s'especifica a les transparències de DTS
getAccuracy :: [(String, String)] -> Float
getAccuracy x = modals / totals
	where
		modals = fromIntegral $ foldl (+) 0 (map (getModals) classes)
		totals = fromIntegral $ length x
		classes = map (map (fst)) . separateByValue $ x
		

--Obté una llista de Strings i fa una llista per cada tipus de valor
separateByValue :: [(String, String)] -> [[(String, String)]]
separateByValue [] = []
separateByValue (x:xs) = (lx:resta)
	where
		lx = [xx | xx <- x:xs, snd xx == snd x]
		ly = [yy | yy <- x:xs, snd yy /= snd x]
		resta = separateByValue ly

-- Obté el nombre de valors que segueixen la moda de la llista
-- Pre: la llista només té dues classes d'elements
getModals :: [String] -> Int
getModals values = modals
	where
		moda = getModa values
		modals = length $ [x | x <- values, x == moda]
		

-- Obtenim la moda d'una llista de Strings
-- Pre: la llista només té dues classes d'elements
getModa :: [String] -> String
getModa (x:xs)
	| length lx > length ly = x
	| otherwise = y
	where
		lx = [xx | xx <- x:xs, xx == x]
		ly = [yy | yy <- x:xs, yy /= x] 
		y = head ly

-- Obtenim els valors dels atributs dels bolets agrupats per atribut.
-- Afegim la classe del bolet a una tupla amb el valor de l'atribut
-- Afegim el nom de l'atribut a una tupla amb la llista dels seus valors
getAllAttributeValues :: [Bolet] -> [(String, [(String,String)])]
getAllAttributeValues bolets = zipWith (makeTupla) names values_by_column
	where
		names =  getAttributeNames $ head bolets
		num_att = length names
		values = map (getAllClassAndAttributes) bolets 
		flist = [map (!! y) | y <- [0..(num_att - 1)]]
		rlist = repeat values
		values_by_column = zipWith ($) flist rlist 

-- Donats dos elements, en retorna una tupla formada per aquests dos elements
makeTupla :: a -> b -> (a,b)
makeTupla x y = (x,y)

-- Bloc de funcions dedicats a recorrer l'arbre de decisions

-- Funció que recorre recursivament l'arbre de decisions depenent de l'input de l'usuari
-- fins que arriba a una fulla
treeTraversal :: DecisionTree -> IO()
treeTraversal (DecisionTree s []) = do
	putStrLn $ readNode (DecisionTree s [])
treeTraversal (DecisionTree s l) = do
	putStrLn $ readNode (DecisionTree s l)
	branch <- getLine
	let new_tree = getBranch (DecisionTree s l) branch
	treeTraversal new_tree

-- Funció que ensenya una prompt a l'usuari amb informació de l'arrel de l'arbre.
-- Si l'arrel és una fulla, classifica el bolet que està entrant l'usuari.
-- En cas contrari, fa una pregunta sobre l'atribut que representa el node actual.
readNode :: DecisionTree -> String
readNode (DecisionTree s []) = "The mushroom is " ++ s
readNode (DecisionTree s l) = "What is the " ++ s ++ " like? " ++ show values
	where
		values = map (fst) l
		


